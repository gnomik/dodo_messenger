﻿using UnityEngine;

public class Rotator : MonoBehaviour
{
    public Vector3 _rotate;

   	void Update ()
    {
        transform.Rotate(_rotate * Time.deltaTime);
	}
}
