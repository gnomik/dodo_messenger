﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public Transform _menuCamPos;

    Camera _cam;

    Vector3 _startPos;
    Quaternion _startRot;

	Vector3 _vel;
    public float _smooth;
    public float _max;

    private UIController _ingameUi;

    private GameCameraController _camFollow;

    void Start()
    {
        _cam = Camera.main;

        _camFollow = _cam.GetComponent<GameCameraController>();
        _camFollow.ForceUpdate(); 
        _camFollow.enabled = false;

		_startPos = _cam.transform.position;
        _startRot = _cam.transform.rotation;

        _cam.transform.position = _menuCamPos.position;
        _cam.transform.rotation = _menuCamPos.rotation;

        FindObjectOfType<PlayerController>().enabled = false;
        _ingameUi = FindObjectOfType<UIController>();
        _ingameUi.gameObject.SetActive(false);
    }


    public void LateUpdate()
    {
        _cam.transform.position = _menuCamPos.position;
        _cam.transform.rotation = _menuCamPos.rotation;
    }

    public void TransitionToGame ()
    {
        if (this.enabled) {
			StartCoroutine(Anim());
        }
    }

    IEnumerator Anim()
    {
        this.enabled = false;

        var rot = _cam.transform.rotation;

        var totalDist = Vector3.Distance(_cam.transform.position, _startPos);

        while ((_cam.transform.position - _startPos).sqrMagnitude > 0.01f) {
            _cam.transform.position = Vector3.SmoothDamp(_cam.transform.position, _startPos, ref _vel, _smooth, _max);
            print(Vector3.Distance(_cam.transform.position, _startPos));
            Debug.Log(1f - (Vector3.Distance(_cam.transform.position, _startPos) / totalDist));
            _cam.transform.rotation = Quaternion.Lerp(rot, _startRot, 1f - (Vector3.Distance(_cam.transform.position, _startPos) / totalDist));
            yield return null;
        }

        _cam.transform.position = _startPos;
        _cam.transform.rotation = _startRot;

        _camFollow.enabled = true;
        FindObjectOfType<PlayerController>().enabled = true;
        _ingameUi.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
