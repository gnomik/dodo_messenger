﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum CollectibleType {None, Stamina, Life, Time};

public class CollectibleController : MonoBehaviour {

    private bool taken = false;

    public bool Taken
    {
        get
        {
            return taken;
        }
    }

    public float respawnTime = 5f;

    public CollectibleType collectibleType;

    private Collider boxCollider;

    void Awake()
    {
        boxCollider = GetComponent<Collider>();
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up * Time.deltaTime * 30f);

        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one * (taken ? 0 : 1f), Time.deltaTime * 10f);
	}

    public void Take()
    {
        taken = true;
        StartCoroutine(Respawn());
    }

    public void Reset()
    {       
        taken = false;
    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(respawnTime);
        Reset();
    }
}
