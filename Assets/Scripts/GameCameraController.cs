﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class GameCameraController : MonoBehaviour
{
    

    private Vector3 rotationDirection = Vector3.zero;
    private float rotationSpeedHorizontal = 120f;
    private float rotationSpeedVertical = 70f;
    private Transform target;

    //private Vector3 _delta; // between player and camera

    float _distance;

    bool _initialized;

    void Awake()
    {
        if (_initialized) return;
        target = GameObject.FindGameObjectWithTag("Player").transform;
		_distance = Vector3.Distance(transform.position, target.position);
        _initialized = true;
    }
	
    public void ForceUpdate()
    {
        Awake();
        UpdateCam(true);
    }

    private void Update()
    {
        UpdateCam(false);
    }

    // Update is called once per frame
    void UpdateCam (bool snapToPos = false)
    {
        Inputs();
        transform.RotateAround(target.position, Vector3.up, rotationDirection.x * Time.deltaTime * rotationSpeedHorizontal);

        var currentVerticalAngle = transform.rotation.eulerAngles.x;
        var maxNegativeDelta = -Mathf.DeltaAngle(-24f, currentVerticalAngle);
        var maxPositiveDelta = -Mathf.DeltaAngle(45f, currentVerticalAngle);
        rotationDirection.y = Mathf.Clamp(rotationDirection.y, maxNegativeDelta, maxPositiveDelta   );
        transform.RotateAround(target.position, transform.right, rotationDirection.y * Time.deltaTime * rotationSpeedVertical);
        var rayDir = -transform.forward;


        var targetPos = transform.position;

        var layerMask = ~((1 << LayerMask.NameToLayer("Player")) | (1 << LayerMask.NameToLayer("Enemies"))); // all masks except player.
            
        var ray = new Ray(target.position + Vector3.up * 3.2f, rayDir *_distance);

        Debug.DrawRay(ray.origin, ray.direction);

		RaycastHit hit;
        var wasHit = Physics.SphereCast(ray, 0.5f, out hit, _distance, layerMask);
        targetPos = ray.GetPoint(wasHit ? hit.distance : _distance);

        transform.position = Vector3.Lerp(transform.position, targetPos, snapToPos ? 1f : 0.3f);
	}

    void Inputs()
    {
        InputDevice device = InputManager.ActiveDevice;
        InputControl horizontal = device.GetControl(InputControlType.RightStickX);
        InputControl vertical = device.GetControl(InputControlType.RightStickY);

        rotationDirection.x = horizontal.Value * -1f;
        rotationDirection.y = vertical.Value * -1;
    }
}
