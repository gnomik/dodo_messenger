﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundClipItem: System.Object
{
    public AudioClip clip;
    public AudioSource source;
}

public class SoundLibrary : MonoBehaviour {

    public SoundClipItem[] clips;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
