﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CompassController : MonoBehaviour {
    GameController gameController;
    private bool active = false;
    void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        active = gameController.Timer > 0;
        this.transform.localScale = Vector3.Lerp(this.transform.localScale, Vector3.one * (active ? 1f : 0), Time.deltaTime * 5f);
        if (gameController.CurrentFlag != null)
            transform.LookAt(gameController.CurrentFlag.transform.position);
	}
}
