﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum PeopleAllegeance { Neutral, Red, Purple, Orange, Blue, Green, Count };

[ExecuteInEditMode]
public class FlagController : MonoBehaviour {
    public static Color[] PeopleColor = { Color.black, Color.red, new Color(1f,0,.5f), new Color(1f,.5f,0), Color.blue, Color.green, Color.white };

    public PeopleAllegeance allegeance;

    public static Action<bool> EnterFlag;

    private bool isCurrentFlag = false;

    public bool IsCurrentFlag
    {
        get
        {
            return isCurrentFlag;
        }

        set
        {
            isCurrentFlag = value;
            if(aura != null)
                aura.gameObject.SetActive(value);
        }
    }

    MaterialPropertyBlock flagProperties;

    private Transform proximityIndicator;
    private Transform aura;

	void Awake()
    {        
        proximityIndicator = transform.Find("ProximityIndicator");
        aura = transform.Find("Aura");

        if (aura != null)
            aura.gameObject.SetActive(false);

        if (proximityIndicator != null)
            proximityIndicator.gameObject.SetActive(false);
    }

	void Start () {
        UpdateFlagColor();
    }

    void UpdateFlagColor()
    {
        Renderer flagRenderer = transform.Find("Flag").GetComponent<Renderer>();
        flagProperties = new MaterialPropertyBlock();
        flagRenderer.GetPropertyBlock(flagProperties);
        flagProperties.SetColor("_Color", PeopleColor[(int)allegeance]);
        flagRenderer.SetPropertyBlock(flagProperties);        
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && IsCurrentFlag)
        {
            if (proximityIndicator != null)
                proximityIndicator.gameObject.SetActive(true);

            if (EnterFlag != null)
                EnterFlag(true);
        }

    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (proximityIndicator != null)
                proximityIndicator.gameObject.SetActive(false);

            if (EnterFlag != null)
                EnterFlag(false);
        }
    }

    void OnValidate()
    {
        UpdateFlagColor();
        gameObject.name = "Flagpole (" + allegeance.ToString() + ")";
    }
}
