﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public Image Energy;
    public Image Life;

    public Text Timer;

    public Text Sequence;

    public Text Message;

    public Text GameOver;
    public Text Win;

    private Color timerColor;
    private Color sequenceColor;

    PlayerController player;

    GameController gameController;

    IEnumerator showSequenceCoroutine;

    void OnEnable()
    {
        GameController.SequenceGenerated += OnSequenceGenerated;
        PlayerController.Died += OnPlayerDied;
        GameController.OnGameStarted += OnGameStarted;
        GameController.MessageDelivered += OnMessageDelivered;
        GameController.GameEnded += OnGameEnded;
    }

    void OnDisable()
    {
        GameController.SequenceGenerated -= OnSequenceGenerated;
        PlayerController.Died -= OnPlayerDied;
        GameController.OnGameStarted -= OnGameStarted;
        GameController.MessageDelivered -= OnMessageDelivered;
        GameController.GameEnded -= OnGameEnded;
    }

    void OnGameEnded(bool win)
    {
        if (win)
            Win.gameObject.SetActive(true);
        else
            GameOver.gameObject.SetActive(true);

        StartCoroutine(ShowMessage("The game has ended !"));
    }

    void OnMessageDelivered(bool valid)
    {
        if(valid)
        {
            StartCoroutine(ShowMessage("The message has been correctly delivered ! Well done !"));
            transform.Find("Sounds/SuccessJingle").GetComponent<SoundPlayer>().Play();
        }
        else
        {
            StartCoroutine(ShowMessage("You failed miserably ! Try again !"));
            transform.Find("Sounds/Failure").GetComponent<SoundPlayer>().Play();
        }
    }

    void OnGameStarted()
    {
        Timer.enabled = true;
        Sequence.enabled = true;

        GameObject.FindGameObjectWithTag("Tutorial").SetActive(false);
    }

    void OnPlayerDied()
    {
        StartCoroutine(ShowMessage("You died and lose some time !"));
    }

    void OnSequenceGenerated(string sequence)
    {
        if (showSequenceCoroutine != null)
            StopCoroutine(showSequenceCoroutine);
        Sequence.text = "You have to deliver this message to " + gameController.CurrentFlag.GetComponent<FlagController>().allegeance.ToString() + " flag:\n" + sequence;
        showSequenceCoroutine = ShowSequence();
         StartCoroutine(showSequenceCoroutine);
    }

    IEnumerator ShowSequence()
    {

        for(int i=0;i<10;i++)
        {
            sequenceColor.a = i / 10f;
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForSeconds(8f);
        sequenceColor.a = 0;
    }

    void Awake()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
        gameController = GameObject.FindObjectOfType<GameController>();
        sequenceColor = Color.white;
        sequenceColor.a = 0;
        Message.enabled = false;
        Timer.enabled = false;
    }
	// Use this for initialization
	void Start () {
        StartCoroutine(WelcomeMessage());
    }

    IEnumerator WelcomeMessage()
    {
        yield return ShowMessage("Welcome to Dodo Messenger !");
        yield return ShowMessage("The aim of the game is to deliver message of peace");
        yield return ShowMessage("To do so, read the sequence on the screen");
    }
	
	// Update is called once per frame
	void Update () {
        Life.fillAmount = player.Life / 100f;
        Energy.fillAmount = player.Energy / 100f;
        timerColor = gameController.Timer > 30f ? Color.white : Color.red;
        Timer.color = timerColor;
        Timer.text = Mathf.Floor(gameController.Timer / 60f).ToString("00") + ":" + Mathf.Floor(gameController.Timer % 60f).ToString("00");
        Sequence.color = sequenceColor;
    }

    IEnumerator ShowMessage(string message)
    {
        Message.text = message;
        Message.enabled = true;
        yield return new WaitForSeconds(5f);
        Message.enabled = false;
    }
}
