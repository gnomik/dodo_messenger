﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using InControl;

public class PlayerController : MonoBehaviour
{
    //private CharacterController cc;
    private Rigidbody body;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 lastMoveDirection = Vector3.zero;

    private Vector3 horizontalDirection = Vector3.zero;
    private Vector3 verticalDirection = Vector3.zero;

    private float speed = 8f;
    private Transform mainCamera;

    public float Life = 100f;

    public float Energy = 100f;

    private bool isRunning = false;

    private bool isDead = false;

    public int DeathCount = 0;

    private float groundDistance;

    public static Action<bool> CodeType;

    public static Action Died;

    private Animator animator;
	private Transform _model;

    private IEnumerator lastStun;

    private bool canMove = true;

    public bool CanJump
    {
        get
        {
            return Energy > 0;
        }
    }


    ParticleSystem featherParticles;

    ParticleSystem triggerLeftParticles;
    ParticleSystem triggerRightParticles;

    void Awake()
    {
        //cc = GetComponent<CharacterController>();
        body = GetComponent<Rigidbody>();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").transform;
        groundDistance = GetComponent<Collider>().bounds.extents.y;
        animator = GetModel().GetComponent<Animator>();
        featherParticles = transform.Find("FeatherParticles").GetComponent<ParticleSystem>();
        triggerLeftParticles = transform.Find("TriggerLeftParticles").GetComponent<ParticleSystem>();
        triggerRightParticles = transform.Find("TriggerRightParticles").GetComponent<ParticleSystem>();
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isDead)
            return;

        Inputs();
        
        RegenEnergy();
	}

    Transform GetModel ()
    {
        return _model = _model ?? transform.Find("Model");
    }

    void Inputs()
    {
        InputDevice device = InputManager.ActiveDevice;
        InputControl horizontal = device.GetControl(InputControlType.LeftStickX);
        InputControl vertical = device.GetControl(InputControlType.LeftStickY);

        float XKeyboard = Input.GetKey(KeyCode.D) ? 1 : Input.GetKey(KeyCode.A) ? -1 : 0;
        float ZKeyboard = Input.GetKey(KeyCode.W) ? 1 : Input.GetKey(KeyCode.S) ? -1 : 0;

        var cameraForward = mainCamera.transform.forward;
        cameraForward.y = 0f;
        var rot = Quaternion.LookRotation(cameraForward, Vector3.up);


        moveDirection = Vector3.zero;
        moveDirection.x = horizontal.Value + XKeyboard;
        moveDirection.z = vertical.Value + ZKeyboard;

        moveDirection.Normalize();

        moveDirection = rot * moveDirection;

        lastMoveDirection = moveDirection;

        isRunning = device.GetControl(InputControlType.Action3).IsPressed;

        bool jumpButtonPressed = device.GetControl(InputControlType.Action1).WasPressed || Input.GetKeyDown(KeyCode.Space);
        
        if (canMove && jumpButtonPressed && Energy > 0)
        {
            if (IsGrounded())
                LoseEnergy(10f);
            else
                LoseEnergy(25f);

            animator.SetTrigger("Jump");
            body.AddForce(Vector3.up * Mathf.Sqrt(2f * -2f * Physics.gravity.y), ForceMode.VelocityChange);
            featherParticles.Play();
        }
        
        
        if (device.GetControl(InputControlType.LeftTrigger).WasPressed && canMove)
        {
            animator.SetTrigger("BeakHit");
            transform.Find("Sounds/HeadHit").GetComponent<SoundPlayer>().Play();
            triggerLeftParticles.Play();
            if (CodeType != null)
                CodeType(false);
        }

        if (device.GetControl(InputControlType.RightTrigger).WasPressed && canMove)
        {
            animator.SetTrigger("BeakHit");
            transform.Find("Sounds/HeadHit").GetComponent<SoundPlayer>().Play();
            triggerRightParticles.Play();
            if (CodeType != null)
                CodeType(true);
        }
        

    }

    void RegenEnergy()
    {
        if (!IsGrounded() || isDead)
            return;

        Energy += 40 * Time.deltaTime;
        Energy = Mathf.Clamp(Energy, 0, 100f);
    }

    void LoseEnergy(float delta)
    {
        if (isDead)
            return;

        Energy -= delta;
        Energy = Mathf.Clamp(Energy, 0, 100f);
    }

    void FixedUpdate()
    {
        if (isDead)
            return;

        float finalSpeed = IsGrounded() ? speed : speed * 2f;

        if (isRunning)
            finalSpeed *= 2f;


        if(canMove)
            body.MovePosition(body.position + moveDirection * finalSpeed * Time.fixedDeltaTime);
        

        animator.SetBool("Grounded", IsGrounded());
        animator.SetFloat("Speed", (moveDirection * finalSpeed).magnitude);

        if (moveDirection.magnitude > 0 && isRunning && IsGrounded())
            LoseEnergy(1f);

        if(lastMoveDirection.magnitude > 0)
            transform.Find("Model").rotation = Quaternion.LookRotation(lastMoveDirection);

        //transform.GetChild(0).rotation = Quaternion.LookRotation((transform.position - moveDirection));

    }

    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, groundDistance + .1f);
    }

    public void Hit(float delta)
    {
        if (isDead)
            return;

        Life -= delta;

        featherParticles.Emit(100);
        featherParticles.Play();

        if (lastStun != null)
            StopCoroutine(lastStun);

        if(Life <= 0)
        {
            Die();
        }
        else
        {
            lastStun = Stun(.3f);
            StartCoroutine(lastStun);
        }

        
    }

    IEnumerator Stun(float duration)
    {        
        animator.SetTrigger("Hit");
        canMove = false;
        yield return new WaitForSeconds(duration);
        canMove = true;

    }

    void OnTriggerEnter(Collider collider)
    {        
        if(collider.name == "Water")
        {

            Die();
        }
    }

    void Die()
    {

        isDead = true;        
        isDead = false;
        Life = 100f;
        Energy = 100f;
        canMove = true;
        body.velocity = Vector3.zero;
        body.MovePosition(GameObject.FindGameObjectWithTag("PlayerRespawn").transform.position);

        DeathCount++;

        if (Died != null)
            Died();

    }
}
