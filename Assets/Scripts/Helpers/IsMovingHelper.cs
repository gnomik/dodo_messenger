﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsMovingHelper : MonoBehaviour {

    public bool IsMoving = false;

    public float timeDeltaMeasure = 0.2f;
    public float speedTreshold = 0.001f;

	void Update () {
        StartCoroutine(MoveCheck());
	}

    IEnumerator MoveCheck()
    {
        var p1 = transform.position;
        yield return new WaitForSeconds(timeDeltaMeasure);
        var p2 = transform.position;

        IsMoving = Vector3.Distance(p1, p2) / timeDeltaMeasure > speedTreshold;
    }
}
