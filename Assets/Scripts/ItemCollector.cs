﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollector : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        CollectibleController collectible = other.gameObject.GetComponent<CollectibleController>();
        if (collectible == null || collectible.Taken)
            return;

        switch(collectible.collectibleType)
        {
            case CollectibleType.Stamina:
                break;
            case CollectibleType.Life:
                break;
            case CollectibleType.Time:
                GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().AddTimer(15f);
                break;
        }
        collectible.Take();
    }
}
