﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {
    private Rigidbody body;
    private bool grounded = false;
    void Awake()
    {
        body = GetComponent<Rigidbody>();
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(!grounded)
        {
            transform.LookAt(transform.position + body.velocity);
        }
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if (grounded)
            return;

        grounded = true;
        body.velocity = Vector3.zero;
        body.isKinematic = true;
        StartCoroutine(Disappear());
        if(collision.collider.tag == "Player")
        {
            collision.collider.gameObject.GetComponent<PlayerController>().Hit(5f);
        }
    }

    IEnumerator Disappear()
    {
        yield return new WaitForSeconds(20f);
        Destroy(gameObject);
    }
}
