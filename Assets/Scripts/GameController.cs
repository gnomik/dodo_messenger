﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
//{ Color.black, Color.red, new Color(1f, 0, .5f), new Color(1f, .5f, 0), Color.blue, Color.green };
public class GameController : MonoBehaviour {


    List<bool> lastCodeTyped = new List<bool>();

    public Texture[] PeopleTextures;
     
    bool[] sequence;

    bool[] typedSequence;

    int currentTypeIndex = 0;

    private float timer = 0;

    public float Timer
    {
        get
        {
            return timer;
        }
    }

    int difficulty = 0;

    bool isInCurrentFlag = false;

    bool timeout = false;
    bool validating = false;

    bool gameStarted = false;
    bool gameFinished = false;

    public bool GameStarted
    {
        get
        {
            return gameStarted;
        }
    }

    public static Action<string> SequenceGenerated;
    public static Action OnGameStarted;
    public static Action<bool> MessageDelivered;
    public static Action<bool> GameEnded;

    public int DeathCount = 0;
    public int ValidDeliveryCount = 0;
    public int FailedDeliveryCount = 0;

    public const int MAXIMUM_TRIES = 4;


    private PlayerController playerController;

    private GameObject currentFlag = null;
    public GameObject CurrentFlag
    {
        get
        {
            return currentFlag;
        }
    }

	void Awake()
    {
        playerController = GameObject.FindObjectOfType<PlayerController>();
    }

	void Start () {
        //GenerateSequence();
        
    }

    public void StartGame()
    {
        gameStarted = true;
        GenerateSequence();
        if (OnGameStarted != null)
            OnGameStarted();
    }

    void OnEnable()
    {
        PlayerController.CodeType += OnCodeType;
        FlagController.EnterFlag += OnEnterFlag;
        PlayerController.Died += OnPlayerDied;
    }

    void OnDisable()
    {
        PlayerController.CodeType -= OnCodeType;
        FlagController.EnterFlag -= OnEnterFlag;
        PlayerController.Died -= OnPlayerDied;
    }

    void OnPlayerDied()
    {
        if (timer > 0)
            timer -= 15f;

        timer = Mathf.Max(0, timer);

        DeathCount++;
    }

    void OnEnterFlag(bool isInFlag)
    {
        isInCurrentFlag = isInFlag;
        if (!isInCurrentFlag)
        {
            currentTypeIndex = 0;
        }            
    }

    void CheckGameStartInput()
    {
        if (gameStarted)
            return;

        string lastCode = "";
        if(lastCodeTyped.Count > 3)
        {
            for(int i = lastCodeTyped.Count -4; i < lastCodeTyped.Count; i++)
            {
                lastCode += lastCodeTyped[i] ? "o" : "d";
            }
        }
        if (lastCode == "dodo")
            StartGame();
    }

    void OnCodeType(bool value)
    {
        lastCodeTyped.Add(value);
        CheckGameStartInput();

        if (!isInCurrentFlag || timeout || validating)
            return;

        typedSequence[currentTypeIndex] = value;

        currentTypeIndex++;

        if (currentTypeIndex >= typedSequence.Length)
            ValidateSequence();
    }

    void ValidateSequence()
    {
        validating = true;
        
        bool valid = true;
        for(int i = 0; i < sequence.Length; i++)
        {
            if (sequence[i] != typedSequence[i])
                valid = false;
        }

        if (valid)
            ValidDeliveryCount++;
        else
            FailedDeliveryCount++;

        if (MessageDelivered != null)
            MessageDelivered(valid);

        StartCoroutine(MissionEnd(valid));
        
    }

    void SelectCurrentFlag()
    {
        FlagController[] flags;
        if (CurrentFlag != null)
        {
            flags = GameObject.FindObjectsOfType<FlagController>().Where(f => f.gameObject != CurrentFlag).ToArray();
        } else
        {
            flags = GameObject.FindObjectsOfType<FlagController>().ToArray();
        }
        
        foreach(FlagController flag in flags)
        {
            flag.IsCurrentFlag = false;
        }

        int flagIndex = UnityEngine.Random.Range(0, flags.Length);
        flags[flagIndex].IsCurrentFlag = true;
        currentFlag = flags[flagIndex].gameObject;
    }
	    

	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        if (sequence == null || sequence.Length == 0 || gameFinished)
            return;

        if(!validating)
        {
            if (!timeout)
            {
                timer -= Time.fixedDeltaTime;
                timer = Mathf.Max(0, timer);
            }

            if (Mathf.Approximately(timer, 0) && !timeout)
            {
                timeout = true;
                StartCoroutine(MissionEnd(false));
            }
        }
        else
        {
            timer = 0;
        }
        
        
        
    }

    IEnumerator MissionEnd(bool valid)
    {
        bool willEndGame = false;

        if (ValidDeliveryCount == MAXIMUM_TRIES)
        {
            StartCoroutine(EndGame(true));
            willEndGame = true;
        }
        else if (FailedDeliveryCount == MAXIMUM_TRIES)
        {
            StartCoroutine(EndGame(false));
            willEndGame = true;
        }
            

        if (!willEndGame)
        {
            if(valid)
                difficulty++;

            yield return new WaitForSeconds(3f);

            GenerateSequence();
        }
    }

    IEnumerator EndGame(bool win)
    {
        gameFinished = true;
        yield return null;
        if (GameEnded != null)
            GameEnded(win);
    }

    void GenerateSequence()
    {
        sequence = new bool[2 * (difficulty + 1)];
        typedSequence = new bool[sequence.Length];
        timeout = false;
        validating = false;
        currentTypeIndex = 0;

        timer = Mathf.Max(30f, 90f - (difficulty * 15f));

        for(int i = 0; i < sequence.Length; i++)
        {
            sequence[i] = UnityEngine.Random.value > .5f ? true : false;
        }

        SelectCurrentFlag();
        
        if (SequenceGenerated != null)
            SequenceGenerated(SequenceToString());
          
    }

    string SequenceToString()
    {
        string sequenceString = "";
        for (int i = 0; i < sequence.Length; i++)
        {
            sequenceString += sequence[i] == true ? "O" : "D";
        }
        return sequenceString;
    }

    public void AddTimer(float delta)
    {
        if (!timeout)
            timer += delta;
    }
}
