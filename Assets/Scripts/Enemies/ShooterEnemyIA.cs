﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterEnemyIA : BaseEnemyIA {
    
    private float aimError = 1f;
    public GameObject arrowPrefab;

    public float fireDelayMax = 1f;
    public float firedelayMin = 0.5f;
    private float fireDelayCurrent = .5f;
    private float lastFire = 0;    

	// Use this for initialization
	void Start () {
		
	}

    void FixedUpdate()
    {
        if (!Peaceful && Time.time > lastFire && IsTriggered())
        {
            Fire();
            lastFire = Time.time + fireDelayCurrent;
        }
    }

    void Update()
    {
        //UpdateAnimatorState();
        transform.LookAt(player.transform.position);
        Vector3 rotation = transform.rotation.eulerAngles;
        
        transform.rotation = Quaternion.Euler(0, rotation.y, 0);
    }

    void Fire()
    {
        GameObject newProjectile = Instantiate(arrowPrefab, transform.position + (transform.up * 2f), Quaternion.identity);
        var target = new Vector3(player.transform.position.x + Random.Range(-aimError, aimError), player.transform.position.y + Random.Range(-aimError, aimError), player.transform.position.z + Random.Range(-aimError, aimError));
        newProjectile.GetComponent<Rigidbody>().velocity = BallisticVelocity(target, Random.Range(20f, 80f));
    }
    
    Vector3 BallisticVelocity(Vector3 targetPosition, float angle)
    {
        Vector3 direction = targetPosition - transform.position;
        float h = direction.y;
        direction.y = 0;
        float dist = direction.magnitude;
        float a = angle * Mathf.Deg2Rad;
        direction.y = dist * Mathf.Tan(a);
        dist += h / Mathf.Tan(a);
        float velocity = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        return velocity * direction.normalized;
    }
}
