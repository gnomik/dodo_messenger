﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncerEnemyIA : BaseEnemyIA {

    Quaternion targetRotation;
    float turnSpeed = 0;
    float turnSpeedChange = 20f;

    private float dashDelay = .5f;
    private float nextDash = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!Peaceful && IsTriggered())
        {

            Vector3 direction = (player.transform.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(direction);
            body.rotation = Quaternion.Slerp(body.rotation, lookRotation, Time.deltaTime * 20f);

            if (Time.time > nextDash)
            {
                //body.rotation = lookRotation;
                //Vector3 dashVelocity = Vector3.Scale(transform.forward * -1f, 15f * new Vector3((Mathf.Log(1f / (Time.deltaTime * body.drag + 1)) / -Time.deltaTime), 0, (Mathf.Log(1f / (Time.deltaTime * body.drag + 1)) / -Time.deltaTime)));                
                //body.AddForce(dashVelocity, ForceMode.VelocityChange);
                body.AddRelativeForce(Vector3.forward * 20f, ForceMode.Impulse);
                //if (PlayerDistance() < 4f)
                //    nextDash = Time.time + .1f;
                //else
                nextDash = Time.time + Random.Range(dashDelay, dashDelay * 2f);
            }

        }
        else
        {
            Vector3 direction = (startingPosition - transform.position).normalized;
            body.rotation = Quaternion.LookRotation(direction);

            body.MovePosition(transform.position + transform.forward * Time.deltaTime);
            
        }
        UpdateAnimatorState();
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Player")
        {
            if(collision.relativeVelocity.magnitude > 20f)
            {
                collision.collider.GetComponent<PlayerController>().Hit(5f);
            }
        }
    }
}
