﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemyIA : MonoBehaviour {

    public PeopleAllegeance Allegeance;
    protected Rigidbody body;
    protected PlayerController player;
    public float TriggerDistance = 60f;
    protected Vector3 startingPosition;
    private Animator animator;
    private MaterialPropertyBlock properties;
    private Renderer helmetRenderer;
    protected bool previousTriggeredState = false;

    public bool Peaceful = false;

    private GameController gameController;
         
    void OnEnable()
    {
        GameController.MessageDelivered += OnMessageDelivered;
    }

    void OnDisable()
    {
        GameController.MessageDelivered -= OnMessageDelivered;
    }

    void OnMessageDelivered(bool valid)
    {
        if(valid == false)
        {
            Peaceful = false;
            return;
        }
        else
        {
            if (gameController.CurrentFlag.GetComponent<FlagController>().allegeance == Allegeance)
                Peaceful = true;
            else
                Peaceful = false;
        }
            
    }

    void Awake()
    {
        properties = new MaterialPropertyBlock();
        helmetRenderer = transform.Find("Model/Helmet").GetComponent<SkinnedMeshRenderer>();
        gameController = GameObject.FindObjectOfType<GameController>();
        player = GameObject.FindObjectOfType<PlayerController>();
        body = GetComponent<Rigidbody>();
        startingPosition = transform.position;
        animator = transform.GetComponentInChildren<Animator>();
        helmetRenderer.GetPropertyBlock(properties);
        int allegeanceIndex = (int)Allegeance;
        properties.SetTexture("_MainTex", gameController.PeopleTextures[allegeanceIndex]);
        helmetRenderer.SetPropertyBlock(properties);
    }

    protected float PlayerDistance()
    {
        return Vector3.Distance(player.transform.position, transform.position);
    }
    
    protected bool IsTriggered()
    {
        return PlayerDistance() < TriggerDistance;
    }

    protected void UpdateAnimatorState()
    {
        if(previousTriggeredState != IsTriggered())
        {
            animator.SetTrigger("Triggered");
        }

        animator.SetFloat("Speed", body.velocity.magnitude);

        previousTriggeredState = IsTriggered();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
