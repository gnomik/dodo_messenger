﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogIA : MonoBehaviour {

    public float minDistanceFromTarget = 1;

    public Transform homeAnchor;
    public float maxDistanceFromAnchor = 20;
    public float minDistanceFromAnchor = 4;

    public float chaseTargetMinDistance = 8;
    public float jumpIntensity = 2;

    public float attackDuration = 1.5f;
    public float attackCooldown = 0.5f;

    private GameObject player;
    private IsMovingHelper isMovingHelper;
    private bool isReturningHome = false;

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        isMovingHelper = isMovingHelper ?? gameObject.AddComponent<IsMovingHelper>();
        if (homeAnchor == null && transform.parent != null) homeAnchor = transform.parent;
    }
	
	void Update () {
        if (!isMovingHelper.IsMoving)
        {
            Vector3 direction;
            if (Vector3.Distance(transform.position, player.transform.position) < minDistanceFromTarget)
            {
                // TODO : Attttttacckkkkkkkk !
                StartCoroutine(AttackTarget());
            }
            else
            {
                float distanceFromHome = homeAnchor == null ? 0 : Vector3.Distance(transform.position, homeAnchor.position);
                if (distanceFromHome < minDistanceFromAnchor)
                {
                    isReturningHome = false;
                }
                else if (distanceFromHome > maxDistanceFromAnchor)
                {
                    Debug.Log("Frog too far from home, going back");
                }

                if (isReturningHome || distanceFromHome > maxDistanceFromAnchor)
                {
                    // return to anchor spot
                    isReturningHome = true;
                    direction = (homeAnchor.position - transform.position);
                }
                else if (Vector3.Distance(transform.position, player.transform.position) < chaseTargetMinDistance)
                {
                    // Go toward player
                    direction = (player.transform.position - transform.position);
                }
                else
                {
                    // Random wandering
                    direction = new Vector3(Random.Range(-1, 1), Random.Range(0.8f, 3.5f), Random.Range(-1, 1));
                }
                Jump(direction);
            }
        }
	}

    void Jump(Vector3 direction)
    {
    //    Debug.Log("Frog jumping");
        direction.Normalize();
        direction.y = Mathf.Clamp(direction.y, 0.3f, 1.9f) + Random.Range(0, 0.8f);
        direction *= jumpIntensity;
        transform.GetComponent<Rigidbody>().AddForce(direction, ForceMode.Impulse);
    }

    IEnumerator AttackTarget()
    {
        Debug.Log("Frog prepare to attack player...");
        yield return new WaitForSeconds(attackDuration);
        TryHitPlayer();
        yield return new WaitForSeconds(attackCooldown);
    }

    void TryHitPlayer()
    {
        if (Vector3.Distance(transform.position, player.transform.position) > minDistanceFromTarget)
        {
            Debug.Log("player is too far, frog missed");
        }
        else
        {
            Debug.Log("player is hit by frog!");
        }
    }
}
