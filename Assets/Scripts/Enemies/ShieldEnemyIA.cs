﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldEnemyIA : MonoBehaviour {
    public float rotationSpeed = 0.05f;
    public float timeToTurnSeconds = 2;
    public float timeToWaitSeconds = 1.5f;

    private GameObject thePlayer;

    // Use this for initialization
    void Start() {
        thePlayer = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update() {
        StartCoroutine(StupidTurnAround());
    }

    IEnumerator StupidTurnAround()
    {
        while (true)
        {
            yield return TurnRight(timeToTurnSeconds);
            yield return TurnLeft(timeToTurnSeconds);
            yield return new WaitForSeconds(timeToWaitSeconds);

            yield return TurnLeft(timeToTurnSeconds);
            yield return TurnRight(timeToTurnSeconds);
            yield return new WaitForSeconds(timeToWaitSeconds);

        }
    }

    IEnumerator TurnRight(float seconds)
    {
        var delay = 0f;
        while (delay < seconds)
        {
            TurnHorizontally(-rotationSpeed);
            delay += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator TurnLeft(float seconds)
    {
        var delay = 0f;
        while (delay < seconds)
        {
            TurnHorizontally(+rotationSpeed);
            delay += Time.deltaTime;
            yield return null;
        }
    }

    void TurnHorizontally(float degrees)
    {
        transform.Rotate(0, degrees, 0);
    }

}
