﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using System.Linq;

public class StunAction : MonoBehaviour
{
    public float _forceScale = 50f;
    public AnimationCurve _focreEnveloppe;
	public float _duration = 0.5f;
	public float _radius = 4f;
	public float _uplift = 2f;

    float _lastPressed = -1000f;

    void Update ()
    {
        InputDevice device = InputManager.ActiveDevice;
        InputControl button = device.GetControl(InputControlType.Action3);

        if (button.IsPressed && button.HasChanged) {
            if (Time.time - _lastPressed < 0.5f && TrySpendEnergy(50)) {
                StartCoroutine(Stun ());
                _lastPressed = -1000f;
            } else {
				_lastPressed = Time.time;
            }
        }
    }

    IEnumerator Stun ()
    {
        Debug.Log("STUNNING !!!");
        var enemies = Physics.OverlapSphere(transform.position, _radius, 1 << LayerMask.NameToLayer("Enemies"));

        Rigidbody[] rbs = enemies.Select(e => e.GetComponent<Rigidbody>()).Where(r => r != null).ToArray();

        GetComponent<ParticleSystem>().Play();

        var startTime = Time.time;

        while (Time.time - startTime < _duration)
        {
			yield return new WaitForFixedUpdate();

            var envelope = _focreEnveloppe.Evaluate(Time.time - startTime);

            Debug.Log("I wanna stun");

            foreach (var rb in rbs)
            {
                Debug.Log("Currently stunnin");

                rb.AddExplosionForce(_forceScale * envelope * Time.deltaTime, transform.position, _radius, _uplift * envelope * Time.deltaTime, ForceMode.Acceleration);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, _radius);
    }

    bool TrySpendEnergy(float amount)
    {
        var player = transform.parent.GetComponentInChildren<PlayerController>();
        if (player.Energy < amount) {
            return false;
        }
        player.Energy -= amount;
        return true;
    }
}
